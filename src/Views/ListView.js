import React, { useState, useEffect } from 'react';

import Book from '../Components/Book'
import { Link } from 'react-router-dom'
import { getBooks } from '../Networking'


const ListView = () => {

    const [orders, setOrders] = useState({ count: 0, sum: 0 })
    const [books, setBooks] = useState(null);

    const orderTrack = (e) => {
        const isChecked = e.target.checked;
        const value = Number(e.target.attributes.customvalue.value);
        let { count, sum } = orders;
        if (isChecked) {
            count++;
            sum += value;
        } else {
            count--;
            sum -= value;
        }
        setOrders({ count: count, sum: sum });
    }



    useEffect(() => {
        const controller = new AbortController();
        getBooks(setBooks, controller);


        return () => {
            controller.abort();
        }
    }, [books]);



    return <div>
        <h2>ListView</h2>
        <div>
            <Link to={"/new"}>Add new book</Link>
        </div>
        {books ? <table>
            <thead>
                <tr>
                    <th colSpan="5">Bookstore</th>
                </tr>
            </thead>
            <tbody>
                {books.map(book => (
                    <tr key={book.bookId}>
                        <td><input type="checkbox" onChange={orderTrack} customvalue={book.price}></input></td>
                        <td><Book data={book} /></td>
                        <td><Link to={{ pathname: "/edit", data: { book } }}>Edit</Link></td>
                    </tr>
                ))}
            </tbody>
            <tfoot>
                <tr>
                    <th>Count:</th>
                    <td>{orders.count}</td>
                    <th>Sum:</th>
                    <td>{Math.abs(orders.sum.toFixed(2))}</td>
                </tr>
            </tfoot>
        </table> : <h2>No books</h2>}

    </div>
}


export default ListView;