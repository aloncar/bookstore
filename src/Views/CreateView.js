import React, { useState } from 'react'

import Book from '../Components/Book'
import ErrorMessages from '../Components/ErrorMessages'
import { createBook } from '../Networking'

const CreateView = (props) => {
    const { history } = props
    let updatedData = null;
    const emptyBook = { title: "", price: "", author: "" };

    const getUpdatedData = (update) => {
        updatedData = update
    }

    const [messages, setMessages] = useState({ title: { isError: false }, price: { isError: false }, author: { isError: false } })


    return <div>
        <h2>CreateView</h2>
        <Book data={emptyBook} modifier={getUpdatedData} />
        <ErrorMessages messages={messages} />
        <div>
            <br />
            <button onClick={history.goBack}>Cancel</button>
            <br />
            <button onClick={() => {
                let noErrors = true;
                if (updatedData) {
                    for (const key in updatedData) {
                        if (!updatedData[key] && key !== 'id') {
                            noErrors = false;
                            let temp = { ...messages };
                            temp[key].isError = true;
                            setMessages(temp);

                        } else if (key !== 'id') {
                            let temp = { ...messages };
                            temp[key].isError = false;
                            setMessages(temp);
                        }
                    }
                    if (noErrors) {
                        createBook(updatedData)
                        history.goBack();
                    }
                }
            }}>Create</button>
        </div>
    </div>
}


export default CreateView;