import React, { useState } from 'react'
import Book from '../Components/Book'
import ErrorMessages from '../Components/ErrorMessages'
import { editBook } from '../Networking'

const EditView = (props) => {
    const { location, history } = props
    let updatedData = null;

    const getUpdatedData = (update) => {
        updatedData = update
    }

    const [messages, setMessages] = useState({ title: { isError: false }, price: { isError: false }, author: { isError: false } })


    return <div>
        <h2>EditView</h2>
        <Book data={location.data.book} modifier={getUpdatedData} />
        <ErrorMessages messages={messages} />
        <div>
            <br />
            <button onClick={history.goBack}>Cancel</button>
            <br />
            <button onClick={() => {
                let noErrors = true;
                if (updatedData) {
                    for (const key in updatedData) {
                        if (!updatedData[key] && key !== 'id') {
                            noErrors = false;
                            let temp = { ...messages };
                            temp[key].isError = true;
                            setMessages(temp);

                        } else if (key !== 'id') {
                            let temp = { ...messages };
                            temp[key].isError = false;
                            setMessages(temp);
                        }
                    }
                    if (noErrors) {
                        editBook(updatedData)
                        history.goBack();
                    }
                }
            }}>Edit</button>
        </div>
    </div>
}

export default EditView;
