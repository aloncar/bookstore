import React, { Fragment, useEffect, useState } from 'react'


const Book = ({ data, modifier }) => {

    const isModifiable = !!modifier;

    const [id, setId] = useState(data.bookId);
    const [title, setTitle] = useState(data.title);
    const [author, setAuthor] = useState(data.author);
    const [price, setPrice] = useState(data.price);

    const regexPrice = new RegExp(/^(0|[1-9]\d*)(\.\d+)?$/);

    const update = {
        id: id,
        title: title,
        author: author,
        price: regexPrice.test(price) ? price : null,
    }

    useEffect(() => {
        if (isModifiable) {
            modifier(update);
        }

    })


    return <Fragment>
        {!isModifiable && <>
            <label>Id:</label>
            <input readOnly={true} placeholder="id" value={id} onChange={(e) => {
                const value = e.target.value;
                setId(Number(value))
            }}>
            </input>
        </>}
        <label>Title:</label>
        <input readOnly={!isModifiable} placeholder="title" value={title} onChange={(e) => {
            const value = e.target.value;
            setTitle(value)
        }}>
        </input>
        <label>Authors:</label>
        <input readOnly={!isModifiable} placeholder="authors" value={author} onChange={(e) => {
            const value = e.target.value;
            setAuthor(value)
        }}>
        </input>
        <label>Price:</label>
        <input readOnly={!isModifiable} placeholder="Price" value={price} inputMode="decimal" onChange={(e) => {
            const value = e.target.value;
            console.log(regexPrice.test(value));
            setPrice(value)
        }}>
        </input>
    </Fragment>
}

export default Book;