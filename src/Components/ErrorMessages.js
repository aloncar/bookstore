import React from 'react';


const ErrorMessages = ({ messages }) => {


    return <div>
        {messages.title.isError && <p>Title field must NOT be empty</p>}
        {messages.author.isError && <p>Author field must NOT be empty</p>}
        {messages.price.isError && <p>Price MUST be a valid positive NUMBER</p>}
    </div>

}

export default ErrorMessages;