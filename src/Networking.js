
const url = 'http://localhost:4567/graphql';

const params = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
}



export const getBooks = (setBooks, controller) => fetch(url, {
    ...params,
    signal: controller.signal,
    body: JSON.stringify({ query: '{ books { title, price, bookId, author } }' },),
})
    .then(res => res.json())
    .then(res => setBooks(res.data.books)).catch(e => {
        if (!controller.signal.aborted) {
            console.log('getBooks error', e);
        }
    })


export const editBook = (data) => {
    fetch(url, {
        ...params,
        body: JSON.stringify({ query: `mutation { editBook(bookId:${data.id},title:"${data.title}",price:${data.price},author:"${data.author}") { title, price, bookId, author } }` },),
    })
        .then(res => res.json())
        .then(res => console.log('book-edited', res.data)).catch(e => {
            console.log('editBook error', e)
        });
}

export const createBook = (data) => {
    fetch(url, {
        ...params,
        body: JSON.stringify({ query: `mutation { createBook(title:"${data.title}",price:${data.price},author:"${data.author}") { title, price, bookId, author } }` },),
    })
        .then(res => res.json())
        .then(res => console.log('book-created', res.data)).catch(e => {
            console.log('createBook error', e)
        });
}