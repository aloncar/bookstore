import React from 'react';
import ReactDOM from 'react-dom';

import ListView from './Views/ListView'
import EditView from './Views/EditView'
import CreateView from './Views/CreateView'
import { Route, BrowserRouter } from 'react-router-dom'

const routes = (
  <BrowserRouter>
    <Route exact path="/" component={ListView} />
    <Route exact path="/edit" component={EditView} />
    <Route exact path="/new" component={CreateView} />
  </BrowserRouter>
)




ReactDOM.render(
  routes,
  document.getElementById('root')
);

