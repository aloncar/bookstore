After starting graphql server from [https://github.com/alacrity-law/apollo-test-app](https://github.com/alacrity-law/apollo-test-app) repository on your local machine.

In the project directory, you can run:

### `npm install`
to add dependencies, and:

### `npm start`

to run the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
